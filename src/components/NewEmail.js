import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import { Api } from '../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: '1ch',
        width: '25ch',
      },
    },
  }));


export default function NewEmail(){
    const classes = useStyles();
    const [domain, setDomain] = useState()
    const [description, setDescription] = useState()

    async function handleSubmit(event){
        event.preventDefault();

        const data = new FormData();
        data.append('domain', domain);
        data.append('description',description);

        await Api.post(`/domains`, data)
        console.log(data);
    }

  return (
    <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
      <TextField id="standard-basic" label="Domain" onChange={event => setDomain(event.target.value)}/>
      <TextField id="standard-basic" label="Description" onChange={event => setDescription(event.target.value)}/>
      <Button variant="contained" color="primary" onClick={handleSubmit}>
        Adicionar
      </Button>
    </form>
  );
    
}