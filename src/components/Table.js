import { useEffect, useState } from "react";
import { Api } from "../services/api";
import { DataGrid } from '@material-ui/data-grid';
import NewEmail from "./NewEmail";



const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'domain', headerName: 'Domain', width: 130 },
    { field: 'description', headerName: 'Description', width: 130 }, 
  ]


function Table() {
  const [resposta, setResposta] = useState([])


  useEffect(() => {
    const buscaApi = async function (){
      try {
        const response = await Api.get('/domains')
        setResposta(response.data)
        console.log(response.data);
      } catch (error) {
        console.log(error);
      }
    }
    buscaApi()
    
  },[])
  
  return (
      <>
    <div style={{ height: 400, width: '100%' }}>
    <DataGrid rows={resposta}  columns={columns} pageSize={5} checkboxSelection />
    
  </div>
  <div>
      <NewEmail></NewEmail>
  </div>
  </>
  
  );
}
export default Table;